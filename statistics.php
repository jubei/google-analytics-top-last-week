<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';

$analytics = initializeAnalytics();
$response = getReport($analytics);

$data = customParseResults($response);

for ($i = 0; $i < 10; $i++) {
    echo $data[$i]['uniquePageviews'];
    echo "\t\t";
    echo $data[$i]['ga:pagePath'];
    echo "\n";
}


/**
 * Initializes an Analytics Reporting API V4 service object.
 *
 * @return An authorized Analytics Reporting API V4 service object.
 */
function initializeAnalytics()
{

    // Use the developers console and download your service account
    // credentials in JSON format. Place them in this directory or
    // change the key file location if necessary.
    $KEY_FILE_LOCATION = __DIR__ . '/service-account-credentials.json';

    // Create and configure a new client object.
    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $analytics = new Google_Service_AnalyticsReporting($client);

    return $analytics;
}


/**
 * Queries the Analytics Reporting API V4.
 *
 * @param service An authorized Analytics Reporting API V4 service object.
 * @return The Analytics Reporting API V4 response.
 */
function getReport($analytics) {

    // Replace with your view ID, for example XXXX.
    $VIEW_ID = "123456789";

    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("7daysAgo");
    $dateRange->setEndDate(date("Y-m-d", strtotime('Monday this week')));

    // Create the Metrics object.
    $sessions = new Google_Service_AnalyticsReporting_Metric();
    $sessions->setExpression("ga:uniquePageviews");
    $sessions->setAlias("uniquePageviews");

    // Create the Dimension object.
    $dimention = new Google_Service_AnalyticsReporting_Dimension();
    $dimention->setName("ga:pagePath");

    $dimention2 = new Google_Service_AnalyticsReporting_Dimension();
    $dimention2->setName("ga:pageTitle");

    $orderby = new Google_Service_AnalyticsReporting_OrderBy();
    $orderby->setFieldName("ga:uniquePageviews");
    $orderby->setOrderType("VALUE");
    $orderby->setSortOrder("DESCENDING");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($VIEW_ID);
    $request->setDateRanges($dateRange);
    $request->setMetrics(array($sessions));

    $request->setDimensions(array($dimention,$dimention2));

    $request->setOrderBys($orderby);

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}


function customParseResults($reports) {
    $data = array();

    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
        $report = $reports[ $reportIndex ];
        $header = $report->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows = $report->getData()->getRows();

        for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
            $row = $rows[ $rowIndex ];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();

            $entry2 = array();

            for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                $entry2[$dimensionHeaders[$i]] = $dimensions[$i];
            }

            for ($j = 0; $j < count($metrics); $j++) {
                $values = $metrics[$j]->getValues();
                for ($k = 0; $k < count($values); $k++) {
                    $entry = $metricHeaders[$k];
                    $entry2[$entry->getName()] = $values[$k];
                }
            }
            
            $data[] = $entry2;
        }
    }

    return $data;
}


