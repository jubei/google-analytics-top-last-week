# google-analytics-top-last-week

Grab a list of paths unique-visited mostly last week.

# usage

1. Clone this
2. Create Google Cloud project
3. Add service account to Google Cloud Project and download service-account-credentials.json
4. Add service account bot to Google Analytics. Write down ViewID
5. Use ViewID and JSON to setup